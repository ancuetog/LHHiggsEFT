# LHHiggsEFT

This package just gathers some of the needed input to generate ggHZ and ggH events with the SMEFTatNLO package. 

It needs an installation of MadGraph, Pythia and Rivet.

# Models
The model used is:
[SMEFTatNLO](https://feynrules.irmp.ucl.ac.be/attachment/wiki/SMEFTatNLO/SMEFTatNLO_U2_2_U3_3_cG_4F_LO_UFO.20190812.tgz)

Only interference terms are generated. In the examples here it is done through restriction cards where only one paramater
is set to a value different from 0.

The cards are generated with the scripts in the CreateRestrictionCard_NLO folder.


# Processes
ggZH:

The parameters (in the notation of SMEFTatNLO) that impacts the ggZH process are:

ctG, cpDC, cpd, c3pl1, c3pl2, cpqi, cpQ3, c3pQ3, cpu, cpt, cpd, ctp


The script runggzh_nomerge.sh has the settings that are used to run. It creates a .txt file for each variatiation
that is then given to MadGraph to generatre hepmc events.


TODO: Update for ggH and Hyy

# Rivet routine
The rivet routine used to get the .root and .yoda files is the public one for STXS. The one in the Rivet/ directory
has some modifications to include some other variables.
To run it I use the runRivet_local.sh file, that splits the jobs in bunches of 10k events (150k are generated) and
then use the mergefile.sh to merge the outputs.

# Plotting and parametrization

TODO: Include scripts for getting the parametrization in STXS bins and plot some distributions.