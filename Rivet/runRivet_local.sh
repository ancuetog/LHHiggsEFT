var=$1 # EFT variation
maxevents=10000
for job in {0..14}
do
    mkdir dir_np0_run${var}_${job}
    cp Rivet*so dir_np0_run${var}_${job} && cd dir_np0_run${var}_${job}
    
    nohup rivet --analysis=HiggsTemplateCrossSections /userdata3/acueto/MadGraph/MG5_aMC_v2_6_4/BB_ggHz_NP0_NLO_${var}/Events/run_01/tag_1_pythia8_events.hepmc.gz --pwd --nevts $maxevents --nskip ${job}0000 &> running_rivet_np0_${var}_${job}.out &
    
    cd ..
    
done
