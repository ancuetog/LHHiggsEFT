for vars in ctG cpDC cpd c3pl1 c3pl2 cpqi cpQ3 c3pQ3 cpu cpt cpd ctp; do
    value=2
        if [ "$vars" == "SM" ]
        then
            value=0
        fi
        echo $value
	
	myfilename=run${vars}_nomerge.txt
	if [ "$vars" == "SM" ]
	then
	    echo "import model SMEFTatNLO_U2_2_U3_3_cG_4F_LO_UFO-NLO" >> $myfilename
	else
	    echo "import model SMEFTatNLO_U2_2_U3_3_cG_4F_LO_UFO-NLO_"$vars >> $myfilename
	fi
	echo "generate g g > h z [QCD] NP^2=="$value" " >> $myfilename
	echo "output CC_ggHz_NP0_NLO_"${vars}  >> $myfilename
	echo "launch" >> $myfilename
	echo "shower=PYTHIA8" >> $myfilename
	echo "0" >> $myfilename
	echo "set nevents 150000" >> $myfilename
#	echo "set lhaid 303400"
	echo "set scale 125." >> $myfilename
	echo "set MW0 80.387000" >> $myfilename
	echo "set WH 4.070000e-03" >> $myfilename
#	echo "set ickkw 0" >> $myfilename
#	echo "set xqcut 0.0" >> $myfilename
#	echo "set ktdurham 30." >> $myfilename
#	echo "set ptj 30." >> $myfilename
#	echo "set dparameter 0.4" >> $myfilename
#	echo "set Merging:doKTMerging  on" >> $myfilename
#	echo "set Merging:TMS 30" >> $myfilename
#	echo "set Merging:nJetMax 1" >> $myfilename
#	echo "set Merging:Process '"'pp>hz'"' " >> $myfilename
#	echo "set Merging:Dparameter 0.4" >> $myfilename
#	echo "set Merging:mayRemoveDecayProducts on" >> $myfilename
	echo "0">> $myfilename
	
        ./bin/mg5_aMC $myfilename 

done


