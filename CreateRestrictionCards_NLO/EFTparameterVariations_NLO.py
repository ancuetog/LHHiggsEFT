def varyEFTParam(par, val):

    print "Vary parameter nb. ", par

    non_zero_params = dict()


    if par == 0:
        print "Compute SM-limit (all EFT param set to 0)."
    elif par == 102:
        non_zero_params={'cpDC':val}
    elif par == 103:
        non_zero_params={'cpWB':val}
    elif par == 104:
        non_zero_params={'cdp':val}
    elif par == 105:
        non_zero_params={'cp':val}
    elif par == 106:
        non_zero_params={'cWWW':val}
    elif par == 107:
        non_zero_params={'cG':val}
    elif par == 108:
        non_zero_params={'cpG':val}
    elif par == 109:
        non_zero_params={'cpW':val}
    elif par == 110:
        non_zero_params={'cpBB':val}

    elif par == 201:
        non_zero_params={'cpl1':val}
    elif par == 202:
        non_zero_params={'cpl2':val}
    elif par == 203:
        non_zero_params={'cpl3':val}
    elif par == 204:
        non_zero_params={'c3pl1':val}
    elif par == 205:
        non_zero_params={'c3pl2':val}
    elif par == 206:
        non_zero_params={'c3pl3':val}
    elif par == 207:
        non_zero_params={'cpe':val}
    elif par == 208:
        non_zero_params={'cpmu':val}
    elif par == 209:
        non_zero_params={'cpta':val}
    elif par == 210:
        non_zero_params={'cpqMi':val}
    elif par == 211:
        non_zero_params={'cpq3i':val}
    elif par == 212:
        non_zero_params={'cpQ3':val}
    elif par == 213:
        non_zero_params={'cpQM':val}
    elif par == 214:
        non_zero_params={'cpu':val}
    elif par == 215:
        non_zero_params={'cpt':val}
    elif par == 216:
        non_zero_params={'cpd':val}
    elif par == 219:
        non_zero_params={'ctp':val}
    elif par == 222:
        non_zero_params={'ctZ':val}
    elif par == 223:
        non_zero_params={'ctW':val}
    elif par == 224:
        non_zero_params={'ctG':val}

        #need to check from here onwards if agree with new model nomenclature
    elif par == 301:
        non_zero_params={'cQq83':val}
    elif par == 302:
        non_zero_params={'cQq81':val}
    elif par == 303:
        non_zero_params={'cQu8':val}
    elif par == 304:
        non_zero_params={'ctq8':val}
    elif par == 306:
        non_zero_params={'cQd8':val}
    elif par == 307:
        non_zero_params={'ctu8':val}
    elif par == 308:
        non_zero_params={'ctd8':val}
    elif par == 310:
        non_zero_params={'cQq13':val}
    elif par == 311:
        non_zero_params={'cQq11':val}
    elif par == 312:
        non_zero_params={'cQu1':val}
    elif par == 313:
        non_zero_params={'ctq1':val}
    elif par == 314:
        non_zero_params={'cQd1':val}
    elif par == 316:
        non_zero_params={'ctu1':val}
    elif par == 317:
        non_zero_params={'ctd1':val}
    elif par == 319:
        non_zero_params={'cQQ8':val}
    elif par == 320:
        non_zero_params={'cQQ1':val}
    elif par == 321:
        non_zero_params={'cQt1':val}
    elif par == 323:
        non_zero_params={'ctt1':val}
    elif par == 325:
        non_zero_params={'cQt8':val}

    elif par == 401:
        non_zero_params={'cQlM1':val}
    elif par == 402:
        non_zero_params={'cQlM2':val}
    elif par == 403:
        non_zero_params={'cQl31':val}
    elif par == 404:
        non_zero_params={'cQl32':val}
    elif par == 405:
        non_zero_params={'cQe':val}
    elif par == 406:
        non_zero_params={'cQmu':val}
    elif par == 407:
        non_zero_params={'ctl1':val}
    elif par == 408:
        non_zero_params={'ctl2':val}
    elif par == 409:
        non_zero_params={'cte':val}
    elif par == 410:
        non_zero_params={'ctmu':val}
    elif par == 415:
        non_zero_params={'cQlM3':val}
    elif par == 416:
        non_zero_params={'cQl33':val}
    elif par == 417:
        non_zero_params={'cQta':val}
    elif par == 418:
        non_zero_params={'ctl3':val}
    elif par == 419:
        non_zero_params={'ctta':val}

    elif par == 501:
        non_zero_params={'cll1111':val}
    elif par == 502:
        non_zero_params={'cll2222':val}
    elif par == 503:
        non_zero_params={'cll3333':val}
    elif par == 504:
        non_zero_params={'cll1122':val}
    elif par == 505:
        non_zero_params={'cll1133':val}
    elif par == 506:
        non_zero_params={'cll2233':val}
    elif par == 507:
        non_zero_params={'cll1221':val}
    elif par == 508:
        non_zero_params={'cll1331':val}
    elif par == 509:
        non_zero_params={'cll2332':val}

    else: 
        raise RuntimeError("Param %i not recognised in these jobOptions."%par)

    return non_zero_params
