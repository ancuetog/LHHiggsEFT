import os
from EFTparameterVariations_NLO import varyEFTParam


# Which parameters to modify (default: 0)

#for var in range(201,216):
for var in {219,222,223,224}:
    print var
    non_zero_params=varyEFTParam(var,1.000000e+00)
    newparam=str(non_zero_params).split("'")
    print newparam[1]
    restrictcard = 'restrict_NLO_'+newparam[1]+'.dat'
    param_card=open(restrictcard,'w')
    param_card_default=open('restrict_NLO_v2.dat','r')

    for line in param_card_default:
        if len(line.strip())==0 or line.strip()[0]=='#':
            param_card.write(line)
	    continue
        spl=line.split('#')
        if len(spl)>1 and spl[1].strip().lower() in [p.lower() for p in non_zero_params]:
            param_card.write('   '+spl[0].split()[0]+' '+str(non_zero_params[p])+' #'+spl[1])
        else:
            param_card.write(line)

    param_card.close()
    param_card_default.close()



